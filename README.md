# lab-docker

This is just a pipeline that builds a docker image from Ubuntu:latest and installs maven with openjdk and libgm1.
I needed this for a university project, because we do E2E tests with playwright in maven and the maven images didn't work for us.
