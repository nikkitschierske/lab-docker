# Use an official Ubuntu runtime as a parent image
FROM ubuntu:latest

# Set the working directory in the container
WORKDIR /usr/src/app

# Install necessary packages
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y maven npm netcat openjdk-17-jre-headless openjdk-17-jdk-headless libgbm1
